package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show developer info";
    }

    @Override
    public void execute() {
        if (serviceLocator == null) return;
        System.out.println("[ABOUT]");
        final String name = serviceLocator.getPropertyService().getDeveloperName();
        final String email = serviceLocator.getPropertyService().getDeveloperEmail();
        System.out.println("NAME: " + name);
        System.out.println("E-MAIL: " + email);
    }

    @NotNull
    @Override
    public String name() {
        return "about";
    }

}
