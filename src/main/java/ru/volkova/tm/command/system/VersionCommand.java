package ru.volkova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.volkova.tm.command.AbstractCommand;

public class VersionCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "show application version";
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        if (serviceLocator == null) return;
        System.out.println(serviceLocator.getPropertyService().getApplicationVersion());
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

}
