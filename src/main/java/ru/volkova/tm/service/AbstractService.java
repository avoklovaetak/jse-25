package ru.volkova.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.volkova.tm.api.IRepository;
import ru.volkova.tm.api.IService;
import ru.volkova.tm.entity.AbstractEntity;

import java.util.Optional;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    public AbstractService(@NotNull final IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public @NotNull Optional<E> add(@NotNull final E entity) {
        repository.add(entity);
        return Optional.of(entity);
    }

    @Override
    public void remove(@NotNull final E entity) {
        repository.remove(entity);
    }

}
